<?php


class Core{
    
    
    public function __construct(){
        
        $controller = $_GET['url'];
        
        include_once 'controller/'.$controller . '.php';
        
        $c = new $controller();
        
        if(isset($_GET['method'])){
            $method = $_GET['method'];
            
            if(method_exists($c,$method)){
                $c->$method();
            }else{
                echo "Nem létezik ez a metódus!";
            }
            
        }
        
    }
    
}